


// Available under the MIT License (MIT);
// Based on https://github.com/p3drosola/Backbone.VirtualCollection


import Backbone from 'backbone';

import BackboneVirtualCollection from 'backbone-virtual-collection';

export class DefaultGroupModel extends Backbone.Model {
}

export class DefaultGroupCollection extends Backbone.Collection {
    closeWith(event_emitter) {
        event_emitter.on('close', this.stopListening);
    }
}


export default class BackboneGroupedCollection extends DefaultGroupCollection {

    /**
     * @param {Collection} collection (base collection)
     * @param {Object} options
     *  - {Function} groupby (function that returns a model's group id)
     *
     *  - {[Function]} GroupModel the group model
     *  - {[Function]} GroupCollection the groups collection
     *
     * @return {Collection}
     */
    constructor(collection, options) {
        super([]);
        this.collection = collection;
        this.options = options;
        this._onReset();

        this.listenTo(this.collection, 'add', this._onAdd);
        this.listenTo(this.collection, 'change', this._onAdd);
        this.listenTo(this.collection, 'remove', this._onRemove);
        this.listenTo(this.collection, 'reset', this._onReset);

        if (!this.options.close_with) {
            console.warn("You should provide an event emitter via `close_with`," +
                         " or else the listeners will never be unbound!");
        } else {
            this.listenToOnce(this.options.close_with,
                              'close', this.stopListening);
            this.listenToOnce(this.options.close_with,
                              'destroy', this.stopListening);
        }
    }
    

    /**
     * Handles the reset event on the base collection
     *
     * @param {Object} options
     */
    _onReset(options) {
        let group_ids = _.uniq(this.collection.map(this.options.groupBy));
        this.reset(_.map(group_ids, this._createGroup.bind(this)));
    }


    /**
     * Creates a Group model for a given id.
     *
     * @param {String} group_id
     * @return {Group}
     */
    _createGroup(group_id) {
        let Constructor = this.options.GroupModel || DefaultGroupModel;

        let vc_options = _.extend(this.options.vc_options || {}, {
            filter: (model) => {
                return this.options.groupBy(model) === group_id;
            },
            close_with: this.options.close_with
        });

        let vc = new BackboneVirtualCollection(this.collection, vc_options);
        let group = new Constructor({id: group_id, vc: vc});
        group.vc = vc;
        vc.listenTo(vc, 'remove', _.partial(this._onVcRemove, group));

        return group;
    }

    /**
     * Handles vc removal
     *
     * @param {VirtualCollection} group_collection
     * @param {?} group
     */
    _onVcRemove(groupModel) {
        if (!groupModel.vc.length) {
            this.remove(groupModel);
        }
    }

    /**
     * Handles the add event on the base collection
     *
     * @param {Model} model
     */
    _onAdd(model) {
        let id = this.options.groupBy(model);

        if (!this.get(id)) {
            this.add(this._createGroup(id));
        }
    }

    /**
     * Handles the remove event on the base collection
     *
     * @param  {Model} model
     */
    _onRemove (options, model) {
        let id = this.options.groupBy(model);
        let group = this.get(id);

        if (group && !group.vc.length) {
            this.remove(group);
        }
    }
    
}
